/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  Text,
} from 'react-native';
import Home from './src/screens/containers/home';

const App: () => React$Node = () => {
  return (
    <>
      <Home>
        <Text>headers</Text>
        <Text>buscador</Text>
        <Text>categorias</Text>
        <Text>sugerencias</Text>
      </Home>
    </>
  );
};


export default App;
